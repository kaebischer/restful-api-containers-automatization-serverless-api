import boto3
import json

# Get the service client
dynamodb = boto3.client('dynamodb')

# Create the DynamoDB table
table_name = 'Watches'
existing_tables = dynamodb.list_tables()['TableNames']

# Check if the table already exists
if table_name not in existing_tables:
    table = dynamodb.create_table(
        TableName = table_name,
        KeySchema = [
            {
                'AttributeName': 'sku',
                'KeyType': 'HASH'
            }
        ],
        AttributeDefinitions = [
            {
                'AttributeName': 'sku',
                'AttributeType': 'S'
            }
        ],
        ProvisionedThroughput = {
            'ReadCapacityUnits': 5,
            'WriteCapacityUnits': 5
        }
    )
# Get the service resource
dynamodb = boto3.resource('dynamodb')

# Get the table object
table = dynamodb.Table(table_name)

# Wait until the table exists
table.meta.client.get_waiter('table_exists').wait(TableName=table_name)

# Insert watch items from JSON file
with open("watches.json") as infile:
    infile = infile.read()
    with table.batch_writer() as batch:
        json_file = json.loads(infile)
        for watch_data in json_file:
            watch_item = {
                'sku': watch_data['sku'],
                'type': watch_data['type'],
                'status': watch_data['status'],
                'gender': watch_data['gender'],
                'year': int(watch_data['year']),
                'dial_material': watch_data['dial_material'],
                'dial_color': watch_data['dial_color'],
                'case_material': watch_data['case_material'],
                'case_form': watch_data['case_form'],
                'bracelet_material': watch_data['bracelet_material'],
                'movement': watch_data['movement']
            }
            batch.put_item(watch_item)

full_scan_request = input("Do you want to perform a full scan? (y/N): ")

if full_scan_request == 'y':
    response = table.scan()
    data = response['Items']
    print(data)
