#!/bin/bash
zip lambdas.zip server.py
aws lambda update-function-code --function-name lambdaGetWatch --zip-file fileb://lambdas.zip
aws lambda update-function-code --function-name lambdaPostWatch --zip-file fileb://lambdas.zip
