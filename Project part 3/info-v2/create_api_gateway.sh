#!/bin/bash
RESP=$(aws apigateway import-rest-api --body 'file://info_openapi_v2.yaml')
API=$(python3 get_api_id.py $RESP)

RESP=$(aws apigateway get-resources --rest-api-id $API)

RESOURCE=$(python3 get_resource_id.py $RESP 0)
RESOURCE2=$(python3 get_resource_id.py $RESP 1)

aws apigateway put-integration --region eu-west-1 --rest-api-id $API --resource-id $RESOURCE --http-method GET --type AWS_PROXY --integration-http-method POST --uri arn:aws:apigateway:eu-west-1:lambda:path/2015-03-31/functions/arn:aws:lambda:eu-west-1:$AWS_USER_ID:function:lambdaGetWatch/invocations --content-handling CONVERT_TO_TEXT

aws apigateway put-integration-response --rest-api-id $API --resource-id $RESOURCE --status-code 200 --response-templates application/json="" --http-method GET

aws apigateway put-integration --region eu-west-1 --rest-api-id $API --resource-id $RESOURCE2 --http-method POST --type AWS_PROXY --integration-http-method POST --uri arn:aws:apigateway:eu-west-1:lambda:path/2015-03-31/functions/arn:aws:lambda:eu-west-1:$AWS_USER_ID:function:lambdaPostWatch/invocations --content-handling CONVERT_TO_TEXT

aws apigateway put-integration-response --rest-api-id $API --resource-id $RESOURCE2 --status-code 200 --response-templates application/json="" --http-method POST

aws lambda add-permission --function-name lambdaGetWatch --statement-id watchstatement --action lambda:InvokeFunction --principal apigateway.amazonaws.com --source-arn "arn:aws:execute-api:eu-west-1:$AWS_USER_ID:$API/*/GET/watch/*"

aws lambda add-permission --function-name lambdaPostWatch --statement-id watchstatement --action lambda:InvokeFunction --principal apigateway.amazonaws.com --source-arn "arn:aws:execute-api:eu-west-1:$AWS_USER_ID:$API/*/POST/watch"

echo $API
