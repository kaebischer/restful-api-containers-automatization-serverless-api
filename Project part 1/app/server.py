from flask import Flask, jsonify, abort, request
from flask_basicauth import BasicAuth
from flask_cachecontrol import FlaskCacheControl, cache
import pymysql.cursors
import json
import os


app = Flask(__name__)

# Use of Flask-CacheControl for setting HTTP expiration headers
flask_cache_control = FlaskCacheControl()
flask_cache_control.init_app(app)
EXPIRATION = 3600

# Authentication
app.config['BASIC_AUTH_USERNAME'] = os.environ['HTTP_USER']
app.config['BASIC_AUTH_PASSWORD'] = os.environ['HTTP_PASS']

# Here, we force authentication for our whole website
app.config['BASIC_AUTH_FORCE'] = True

# Basic Auth
basic_auth = BasicAuth(app)


@app.route('/info/v1/watch', methods=['POST'])
def add_watch():
    """Add a new watch in the database given request body values as JSON string.
    """
    # Get JSON data from body.
    w_data = request.get_json()
    if not are_data_valid(w_data):
        abort(400)
    w_sku = w_data['sku']
    w_type = w_data['type']
    w_status = w_data['status']
    w_gender = w_data['gender']
    w_year = int(w_data['year'])
    w_dial_material = w_data['dial_material']
    w_dial_color = w_data['dial_color']
    w_case_material = w_data['case_material']
    w_case_form = w_data['case_form']
    w_bracelet_material = w_data['bracelet_material']
    w_movement = w_data['movement']
    
    # Insert watch into DB.
    insert_query = '''INSERT INTO `watches` (
            `sku`, `type`, `status`, `gender`, `year`, `dial_material`, 
            `dial_color`, `case_material`, `case_form`, `bracelet_material`, 
            `movement`) 
            VALUES ('%s', '%s', '%s', '%s', '%d', '%s', '%s', '%s', '%s', 
            '%s', '%s')''' % (w_sku, w_type, w_status, w_gender, w_year, w_dial_material, w_dial_color, w_case_material, w_case_form, w_bracelet_material, w_movement)
    create_update_delete_db(insert_query)

    return success()

@app.route('/info/v1/watch/<sku>', methods=['GET'])
@cache(max_age=EXPIRATION, public=True)
def get_sku_watch(sku):
    """Returns the watch corresponding to the given sku as JSON string.
    
    :param sku: the identifier of the watch
    
    :type sku: string
    """
    # Get watch from DB by primary key (sku).
    select_query = "SELECT * FROM watches WHERE sku = '%s'" % (sku)
    result_query = read_db_fetchone(select_query)
    if result_query:
        return jsonify(result_query)
    else:
        abort(404)

@app.route('/info/v1/watch/<sku>', methods=['POST'])
def update_watch(sku):
    """Updates an existing watch given a sku, 
    with request body values as JSON string.
    
    :param sku: the identifier of the watch
    
    :type sku: string
    """
    # Verify if the watch exist.
    select_query = "SELECT * FROM watches WHERE sku = '%s'" % (sku)
    result_query = read_db_fetchone(select_query)
    if not result_query:
        abort(404)

    # Get watch's values in JSON.
    w_data = request.get_json()

    if not are_data_valid(w_data):
        abort(400)
    w_sku = w_data['sku']
    w_type = w_data['type']
    w_status = w_data['status']
    w_gender = w_data['gender']
    w_year = int(w_data['year'])
    w_dial_material = w_data['dial_material']
    w_dial_color = w_data['dial_color']
    w_case_material = w_data['case_material']
    w_case_form = w_data['case_form']
    w_bracelet_material = w_data['bracelet_material']
    w_movement = w_data['movement']

    # Update the watch in the DB.
    update_query = '''UPDATE watches SET
        sku = "%s", type = "%s", status = "%s", gender = "%s", 
        year = "%s", dial_material = "%s", dial_color = "%s", 
        case_material = "%s", case_form = "%s", bracelet_material = "%s", 
        movement = "%s" WHERE sku = "%s"''' % (w_sku, w_type, w_status, w_gender, w_year, w_dial_material, w_dial_color, w_case_material, w_case_form, w_bracelet_material, w_movement, sku)
    create_update_delete_db(update_query)
    return success()

@app.route('/info/v1/watch/<sku>', methods=['DELETE'])
def delete_sku_watch(sku):
    """Deletes the watch corresponding to the given sku.
    
    :param sku: the identifier of the watch
    
    :type sku: string
    """
    # Check first if the given watch exits in the DB. If yes, delete it.
    select_query = "SELECT sku FROM watches WHERE sku = '%s'" % (sku)
    result_query = read_db_fetchone(select_query)
    if result_query:
        delete_query = "DELETE FROM watches WHERE sku = '%s'" % (sku)
        create_update_delete_db(delete_query)
        return success()
    else:
        abort(404)

@app.route('/info/v1/watch/complete-sku/<prefix>', methods=['GET'])
@cache(max_age=EXPIRATION, public=True)
def get_by_prefix(prefix):
    """Returns a list of skus corresponding to the given prefix.
    
    :param prefix: prefix of the watch identifier
    
    :type prefix: string
    """
    # Get watch in the DB corresponding to the given prefix.
    select_query = "SELECT sku FROM watches WHERE sku LIKE '%s'" % (prefix + "%")
    result_query = read_db_fetchall(select_query)
    list_result = []
    if result_query:
        for v in result_query:
            list_result.append(v['sku'])
    return jsonify(list_result)

@app.route('/info/v1/watch/find', methods=['GET'])
@cache(max_age=EXPIRATION, public=True)
def find_by_any_criteria():
    """Returns a list of watches corresponding 
    to the given creteria (sku, type, status, gender, year) 
    from request body as JSON string.
    """
    # Get watch's values in JSON. 
    w_data = request.get_json()

    if 'sku' in w_data:
        w_sku = w_data['sku']
    else:
        w_sku = ""
    if 'type' in w_data:
        w_type = w_data['type']
    else:
        w_type = None
    if 'status' in w_data:
        w_status = w_data['status']
    else:
        w_status = None
    if 'gender' in w_data:
        w_gender = w_data['gender']
    else:
        w_gender = None
    if 'year' in w_data:
        w_year = int(w_data['year'])
    else:
        w_year = None

    # Get watch in DB corresponding to any given critera.
    list_result = []
    select_query = "SELECT * FROM watches WHERE sku LIKE '%s'" % (w_sku + "%")
    if w_type:
        select_query += " AND type = '%s'" % (w_type)
    if w_status:
        select_query += " AND status = '%s'" % (w_status)
    if w_gender:
        select_query += " AND gender = '%s'" % (w_gender)
    if w_year:
        select_query += " AND year = '%s'" % (w_year)
    result_query = read_db_fetchall(select_query)
    if result_query:
        list_result = result_query    
    return jsonify(list_result)


# ====================================
# Success and error handling functions
# ====================================
def success():
    """
    Returns a status message and code corresponding 
    to status code 200 (Successful operation) as JSON string.
    """
    message = {
            'status': 200,
            'message': "Successful operation"
    }
    return jsonify(message)

@app.errorhandler(404)
def not_found(e):
    """
    Returns a status message and code corresponding 
    to status code 404 (Watch not found) as JSON string.
    
    :param e: status code
    
    :type e: int
    """
    message = {
            'status': 404,
            'message': "Watch not found",
    }
    return jsonify(message), 404

@app.errorhandler(400)
def server_error(e):
    """
    Returns a status message and code corresponding 
    to status code 400 (Invalid input) as JSON string.
    
    :param e: status code
    
    :type e: int
    """
    message = {
            'status': 400,
            'message': "Invalid input",
    }
    return jsonify(message), 400


# ================
# Helper functions
# ================
def connect_to_db():
    """
    Connects to the database.
    """
    connection = pymysql.connect(host=os.environ['DB_HOST'],
                                user=os.environ['DB_USER'],
                                password=os.environ['DB_PASS'],
                                database=os.environ['DB_DBNAME'],
                                cursorclass=pymysql.cursors.DictCursor)
    if connection != None:
        return connection
    else:
        print("Connection to database failed")

def read_db_fetchone(query):
    """
    Returns specific entry from database as JSON string.
    
    :param query: sql query
    
    :type query: string
    """
    with connection.cursor() as cursor:
        try:
            cursor.execute(query)
            result_query = cursor.fetchone()
        except:
            abort(400)
    return result_query

def read_db_fetchall(query):
    """
    Returns all corresponding entries from database as JSON string.
    
    :param query: sql query
    
    :type query: string
    """
    with connection.cursor() as cursor:
        try:
            cursor.execute(query)
            result_query = cursor.fetchall()
        except:
            abort(400)
    return result_query

def create_update_delete_db(query):
    """
    Creates, updates or deletes entry in database.
    
    :param query: sql query
    
    :type query: string
    """
    with connection.cursor() as cursor:
        try:
            cursor.execute(query)
            connection.commit()
        except:
            abort(400)

def are_data_valid(w_data):
    """
    Returns whether or not the data respects the structure of the database.
    
    :param w_data: watch data
    
    :type w_data: JSON string
    """
    valid_w_type = ['watch', 'chrono']
    valid_w_status = ['old', 'current', 'outlet']
    valid_w_gender = ['man', 'woman']
    valid_length = 255

    # Check string lenghts
    for k, v in w_data.items():
        if k == 'year':
            continue
        if not (len(v) <= valid_length):
            return False
        if k == 'sku' and len(v) == 0:
            return False
    
    # Check watch's type
    if w_data['type'] not in valid_w_type:
        return False

    # Check watch's status
    if w_data['status'] not in valid_w_status:
        return False

    # Check watch's gender
    if w_data['gender'] not in valid_w_gender:
        return False

    # Year should be greater or equal than 1700 (our decision)
    if not (int(w_data['year']) >= 1700):
        return False
    return True


# Main
if __name__ == '__main__':
    connection = connect_to_db()
    app.run(debug=True,host='0.0.0.0')