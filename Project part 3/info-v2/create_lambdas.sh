#!/bin/bash
zip lambdas.zip server.py

aws lambda create-function --function-name lambdaGetWatch --role arn:aws:iam::$AWS_USER_ID:role/WildLambda --runtime python3.8 --handler server.get_watch --zip-file fileb://lambdas.zip 
aws lambda create-function --function-name lambdaPostWatch --role arn:aws:iam::$AWS_USER_ID:role/WildLambda --runtime python3.8 --handler server.post_watch --zip-file fileb://lambdas.zip 
