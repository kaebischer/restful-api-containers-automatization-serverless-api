import json
import sys

json_cut = len(sys.argv) - 1
json_assembled = ''.join(sys.argv[1:json_cut])

injson = json.loads(json_assembled)

method = "GET" if sys.argv[json_cut] == '0' else "POST"

for json_obj in injson['items']:
    if 'resourceMethods' in json_obj:
        if method in json_obj['resourceMethods']:
            print(json_obj['id'])
            exit(0)
