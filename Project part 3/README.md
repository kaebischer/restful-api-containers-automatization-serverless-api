# Cloud Computing 2021 Assignments - Project

## Watch Application
This is a Serverless API written in Python, with a given YAML specifications. It accesses a DynamoDB database of watches (information).

There is one microservice info-v2.

### Getting started
Everything was tested on Ubuntu-mate 20.10 virtual machine. 

#### Requirements
##### For local deployment
We need Docker, Docker-Compose, AWS-CLI and some specific python packages in order to test the application locally:
```shell
sudo apt install docker docker.io docker-compose awscli
cd cloudcomputing-2021-assignments/project3/info-v2-local
pip3 install -r requirements.txt
```

Do not forget to configure your local AWS credential information with the command:
```shell
aws configure
```

The following command is used to run everything to test lambda functions locally:

```shell
sh run.sh
```
These command will perform the following:
- Create a docker network for the connection between the container of Dynamodb and sam environment;
- Run the docker container for Dynamodb;
- Initialize the Dynamodb database;
    - The script will ask for scanning all items present in the database.
- Start in local the Serverless API

##### For AWS deployment
We need AWS-CLI and some specific python packages in order to deploy the application into AWS:
```shell
sudo apt install awscli
cd cloudcomputing-2021-assignments/project3/info-v2
pip3 install -r requirements.txt
```

Do not forget to configure your local AWS credential information with the command:
```shell
aws configure
```

The following commands are used to perform the deployment (do not forget to indicate your AWS user ID in the ```export``` command):

```shell
export AWS_USER_ID=your_aws_user_id
sh deploy.sh
```
These commands will perform the following:
-  The export command in Linux is used for creating environment variables temporarily, so that ID does not appear in the code;
- The deploy script does the following:
    - Create Lambda functions;
    - Create the AWS API Gateway from the OpenAPI specification and bind the REST endpoints to Lambda functions;
    - Initialize the Dynamodb database;
        - The script will ask for scanning all items present in the database.
    - Deploy the public AWS API gateway;

###### For AWS - Update Lambdas
The following commands are used to update Lambda functions in AWS:
```shell
cd cloudcomputing-2021-assignments/project3/info-v2
sh update_lambdas.sh
```

#### Microservices access
- **Local**:
    - http://localhost:3000/info/v2/watch/
- **AWS**:
    - https://dk4w4ctm7e.execute-api.eu-west-1.amazonaws.com/infov2/watch/



### Testing examples
- **Local**:
    - **Method 1**: Run some curl commands 
    in a new terminal window like (examples):

        ```shell
        curl -X GET http://localhost:3000/info/v2/watch/CAR5A91.FT6162
        curl -X POST -H 'Content-Type: application/json' http://localhost:3000/info/v2/watch -d '{"sku": "NEW.TEST.WATCH","type": "watch","status": "old","gender": "man","year": 2021,"dial_material": "string","dial_color": "string","case_material": "string","case_form": "string","bracelet_material": "string","movement": "string"}'
        ``` 

    - **Method 2**: Run the ```test.sh``` script which will run some curl commands for basic testing. 
        - While the API is running, in a new terminal window, we can launch the test script:
        ```shell
        cd cloudcomputing-2021-assignments/project3/info-v2-local
        sh ./doc-and-scripts/test.sh
        ```

        These two commands will perform the following:
        - Move to the project's directory (if not done yet);
        - Run the test script;

        The ```test.sh``` script will perform all necessary curl commands for basic testing and print the output in the file ```output-requests.txt```. The ```test.sh``` script calls ```curl.sh``` script which contains different curl commands. The content of this file is then compared with the expected correct responses from ```correct-requests.txt``` file.

- **AWS**:
    - **Method 1**: Run some curl commands 
    in a terminal window like (examples):

        ```shell
        curl -X GET https://dk4w4ctm7e.execute-api.eu-west-1.amazonaws.com/infov2/watch/CAR5A91.FT6162
        curl -X POST -H 'Content-Type: application/json' https://dk4w4ctm7e.execute-api.eu-west-1.amazonaws.com/infov2/watch -d '{"sku": "NEW.TEST.WATCH","type": "watch","status": "old","gender": "man","year": 2021,"dial_material": "string","dial_color": "string","case_material": "string","case_form": "string","bracelet_material": "string","movement": "string"}'
        ``` 

    - **Method 2**: Run the ```test.sh``` script which will run some curl commands for basic testing. 
        - While the API is running, in a new terminal window, we can launch a scripted test:
        ```shell
        cd cloudcomputing-2021-assignments/project3/info-v2
        ./doc-and-scripts/test.sh
        ```

        These two commands will perform the following:
        - Move to the project's directory (if not done yet);
        - Run the test script;

        The ```test.sh``` script will perform all necessary curl commands for basic testing and print the output in the file ```output-requests.txt```. The ```test.sh``` script calls ```curl.sh``` script which contains different curl commands. The content of this file is then compared with the expected correct responses from ```correct-requests.txt``` file.


### Features & utils
Here are the different modules used to achive the requirements of the project3 (we have listed only new modules not used in the previous part):
- **Boto3**: It is the Amazon Web Services (AWS) Software Development Kit (SDK) for Python, which allows Python developers to write software that makes use of services like Amazon S3 and Amazon EC2. 
- **Awscli**: This package provides a unified command line interface to Amazon Web Services.
- **Aws-sam-cli**: The AWS Serverless Application Model (SAM) is an open-source framework for building serverless applications. It provides shorthand syntax to express functions, APIs, databases, and event source mappings. With just a few lines of configuration, you can define the application you want and model it.
