from flask import Flask, jsonify, abort, make_response
from flask_cachecontrol import FlaskCacheControl, cache_for
import json
import os
import requests_etag_cache
from requests_cache import CachedSession

app = Flask(__name__)

# Use of flask-cachecontrol for setting HTTP expiration headers to 1 hour
flask_cache_control = FlaskCacheControl()
flask_cache_control.init_app(app)
EXPIRATION = 1

# Use of requests_cache to cache all requests locally to improve performance
session = CachedSession()
session.cache.clear()

BASE_URL = os.environ['AWS_S3_URL']

@app.route('/image/v1/watch/<sku>', methods=['GET'])
@cache_for(hours=EXPIRATION)
def get_image_by_sku(sku):
    """Returns a response with the image of the watch with the status code 200 
    or returns a status message and code corresponding
    to status code 404 (Watch not found) as JSON string
    
    :param sku: the identifier of the watch
    
    :type sku: string
    """
    path = BASE_URL + str(sku) + '.png'
    resp = session.get(path)
    # Check if ETag was not already cached
    if not requests_etag_cache.uptodate(resp):
        # Cache ETag
        requests_etag_cache.save(resp)
        resp = session.get(path)
    # Check the HTTP status code of the response
    if resp.status_code == 200:
        final = make_response(resp.content)
        final.headers['Content-Type'] = 'image/png'
        final.headers['ETag'] = requests_etag_cache.get(resp)
        return final
    else:
        abort(404)

# Route needed for Ingress healthy check
@app.route('/', methods=['GET'])
def healthy_check():
    """
    Returns status code 200 for Ingress healthy check.
    It avoids to search for favicon.ico (otherwise 404 status code).
    """
    return make_response('<head><link rel="icon" href="data:,"></head>', 200)

# ====================================
# Error handling function
# ====================================
@app.errorhandler(404)
def not_found(e):
    """
    Returns a status message and code corresponding 
    to status code 404 (Watch not found) as JSON string.
    
    :param e: status code
    
    :type e: int
    """
    message = {
            'status': 404,
            'message': "Watch not found",
    }
    return jsonify(message), 404

# Main
if __name__ == '__main__':
    app.run(host='0.0.0.0', port='1080')