#!/bin/bash 

# Success - GET existant watch
curl -X GET https://dk4w4ctm7e.execute-api.eu-west-1.amazonaws.com/infov2/watch/CAR5A91.FT6162
# Success - POST new watch
curl -X POST -H 'Content-Type: application/json' https://dk4w4ctm7e.execute-api.eu-west-1.amazonaws.com/infov2/watch -d '{"sku": "NEW.TEST.WATCH","type": "watch","status": "old","gender": "man","year": 2021,"dial_material": "string","dial_color": "string","case_material": "string","case_form": "string","bracelet_material": "string","movement": "string"}'
# Success - GET new watch
curl -X GET https://dk4w4ctm7e.execute-api.eu-west-1.amazonaws.com/infov2/watch/NEW.TEST.WATCH

# Error - GET inexistant watch
curl -X GET https://dk4w4ctm7e.execute-api.eu-west-1.amazonaws.com/infov2/watch/INEXISTANT.WATCH
# Error - POST without sku specified
curl -X POST -H 'Content-Type: application/json' https://dk4w4ctm7e.execute-api.eu-west-1.amazonaws.com/infov2/watch -d '{"sku": "","type": "watch","status": "old","gender": "man","year": 2021,"dial_material": "string","dial_color": "string","case_material": "string","case_form": "string","bracelet_material": "string","movement": "string"}'
# Error - POST without type specified
curl -X POST -H 'Content-Type: application/json' https://dk4w4ctm7e.execute-api.eu-west-1.amazonaws.com/infov2/watch -d '{"sku": "FAILED.TEST2.WATCH","type": "","status": "old","gender": "man","year": 2021,"dial_material": "string","dial_color": "string","case_material": "string","case_form": "string","bracelet_material": "string","movement": "string"}'
# Error - POST without status specified
curl -X POST -H 'Content-Type: application/json' https://dk4w4ctm7e.execute-api.eu-west-1.amazonaws.com/infov2/watch -d '{"sku": "FAILED.TEST3.WATCH","type": "watch","status": "","gender": "man","year": 2021,"dial_material": "string","dial_color": "string","case_material": "string","case_form": "string","bracelet_material": "string","movement": "string"}'
# Error - POST without gender specified
curl -X POST -H 'Content-Type: application/json' https://dk4w4ctm7e.execute-api.eu-west-1.amazonaws.com/infov2/watch -d '{"sku": "FAILED.TEST4.WATCH","type": "watch","status": "old","gender": "","year": 2021,"dial_material": "string","dial_color": "string","case_material": "string","case_form": "string","bracelet_material": "string","movement": "string"}'
# Error - POST without year specified
curl -X POST -H 'Content-Type: application/json' https://dk4w4ctm7e.execute-api.eu-west-1.amazonaws.com/infov2/watch -d '{"sku": "FAILED.TEST5.WATCH","type": "watch","status": "old","gender": "man","year": "","dial_material": "string","dial_color": "string","case_material": "string","case_form": "string","bracelet_material": "string","movement": "string"}'