import boto3
import json

def get_watch(event, context):
    """ 
    Returns a response with the watch information as JSON string with the status code 200 
    or returns a status message and code as JSON string: 404 (Watch not found)
    """
    # Get the service client
    dynamodb = boto3.client('dynamodb', endpoint_url='http://dynamodb:8000')

    table_name = 'Watches'
    sku = event['pathParameters']['sku']
    get_response = dynamodb.get_item(TableName=table_name, Key={"sku": {'S': sku}})

    if 'Item' in get_response:
        response_watch = get_response['Item']

        watch_data = {
            'sku': response_watch['sku']['S'],
            'type': response_watch['type']['S'],
            'status': response_watch['status']['S'],
            'gender': response_watch['gender']['S'],
            'year': int(response_watch['year']['N']),
            'dial_material': response_watch['dial_material']['S'],
            'dial_color': response_watch['dial_color']['S'],
            'case_material': response_watch['case_material']['S'],
            'case_form': response_watch['case_form']['S'],
            'bracelet_material': response_watch['bracelet_material']['S'],
            'movement': response_watch['movement']['S']
        }
       
        json_response = get_json_response(200, watch_data)
    else:
        json_response = get_json_response(404, None)
    return json_response


def post_watch(event, context):
    """
    Adds a new watch from body JSON string values and
    returns a status message and code as JSON string: 
    200 (Successfull operation)
    400 (Invalid input) 
    """
    dynamodb = boto3.client('dynamodb', endpoint_url='http://dynamodb:8000')
    dynamodbr = boto3.resource('dynamodb', endpoint_url='http://dynamodb:8000')
    
    table_name = 'Watches'
    table = dynamodbr.Table(table_name)
    body = event['body']
    
    if are_data_valid(json.loads(body)):
        sku = json.loads(body)['sku']
        get_response = dynamodb.get_item(TableName=table_name, Key={"sku": {'S': sku}})

        if 'Item' in get_response:
            json_response = get_json_response(400, None)
        else:
            with table.batch_writer() as batch:
                batch.put_item(json.loads(body))

            json_response = get_json_response(200, None)
    else:
        json_response = get_json_response(400, None)
    return json_response

def are_data_valid(w_data):
    """
    Returns whether or not the data respects the structure and constraints of the database.
    
    :param w_data: watch data
    
    :type w_data: JSON string
    """
    mandatory_w_input = ['sku', 'type', 'status', 'gender', 'year', 'dial_material', 'dial_color', 'case_material', 'case_form', 'bracelet_material', 'movement']
    valid_w_type = ['watch', 'chrono']
    valid_w_status = ['old', 'current', 'outlet']
    valid_w_gender = ['man', 'woman']
    valid_length = 255

    # Check if all inputs are present and valid
    if len(w_data) == len(mandatory_w_input):
        for k, v in w_data.items():
            if k not in mandatory_w_input:
                return False
            if k == 'sku' and len(v) == 0:
                return False
            # If it is year, no length check is needed
            if k == 'year':
                continue
            if not (len(v) <= valid_length):
                return False
        
        # Check watch's type
        if w_data['type'] not in valid_w_type:
            return False

        # Check watch's status
        if w_data['status'] not in valid_w_status:
            return False

        # Check watch's gender
        if w_data['gender'] not in valid_w_gender:
            return False

        # Year should be a digit or an integer and greater or equal than 0
        if type(w_data['year']) is str and str(w_data['year']).isdigit() and (int(w_data['year']) >= 0):
            return True
        elif type(w_data['year']) is int and (w_data['year'] >= 0):
            return True
        else:
            return False
    else:
        return False

# ====================================
# Success and error handling functions
# ====================================
def get_json_response(status, watch_data):
    """
    Returns a status message and code as JSON string: 
    200 (Successful operation), 
    400 (Invalid input), 
    404 (Watch not found), 
    501 (Not implemented)
    """

    if status == 200:
        return make_response(status, watch_data, "Successful operation")
    elif status == 400:
        return make_response(status, watch_data, "Invalid input")
    elif status == 404:
        return make_response(status, watch_data, "Watch not found")
    else:
        return make_response(status, watch_data, "Not implemented")

def make_response(status, watch_data, message):
    """
    Helper function to return a JSON response
    """
    json_response = json.loads('{"isBase64Encoded": false, "statusCode":'+str(status)+', "headers": {"content-type": "application/json"}}')
    if watch_data == None:
        json_response['body'] = json.dumps(dict({"status": status, "message": message})) + "\n"
    else:
        json_response['body'] = json.dumps(watch_data) + "\n"
    return json_response
