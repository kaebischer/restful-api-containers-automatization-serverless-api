# Cloud Computing 2021 Assignments - Project

## Watch Application
This is a RESTful application written in Python/Flask, with a given YAML specifications. It accesses a MySQL Database of watches.
Docker-Compose is used to set up automatically every containers that are needed for the web service such as:
- Python-flask web/app
- MySQL database

#### Getting started

Everything was tested on Ubuntu-mate 20.10 virtual machine. 
##### Requirements
We need Docker and Docker-Compose in order to test our application:
```shell
sudo apt install docker docker.io docker-compose
```

The following commands are used to run everything for in order to test our application:

```shell
git clone https://gitlab.com/d-m-3/cloudcomputing-2021-assignments.git
cd cloudcomputing-2021-assignments/project
./run.sh
```

These three commands will perform the following:
- Copy the project's repository;
- Move to the project's directory;
- Run the application;

The run.sh script will set and run everything to get all necessary docker containers running in order to use the application (mysql, phpmyadmin, flask app).

##### Flask app and PhpMyAdmin access
- flask app: http://localhost:5000/info/v1/
	- *username* = cloud
	- *password* = computing
- phpmyadmin: http://localhost:1080/
	- *username* = watches
	- *password* = watches

#### Testing validation
While the server is running, in a new terminal window, we can launch a scripted test:
```shell
cd cloudcomputing-2021-assignments/project
./doc-and-scripts/test.sh
```

These two commands will perform the following:
- Move to the project's directory (if not done yet);
- Run the test script;

The test.sh script will perform all necessary curl commands and print the output in the file ```output-requests.txt```. The test.sh script calls curl.sh script which contains different curl commands. The content of this file is then compared with the expected correct responses. We try correct and incorrect CREATE, READ, UPDATE and DELETE requests.

#### Features & utils
Here are the different modules used to achive the requirements of the project:
- **Flask**: Flask is a microframework for Python based on Werkzeug, Jinja 2
- **Flask-BasicAuth**: Flask-BasicAuth is a Flask extension that provides an easy way to protect certain views or your whole application with HTTP basic access authentication.
	- It was used to put an HTTP basic access authentication.
- **Flask-CacheControl**: A light-weight library to conveniently set Cache-Control headers on the response.
	- It was used to set the expiration header.
- **PyMySQL**: It is a package that contains a pure-Python MySQL client library, based on PEP 249.
	- It was used to connect and interact with the DB.
- **JSON**: It is a lightweight data interchange format.
	- It was used to tranform value in json format.
- **OS**: This module provides a portable way of using operating system dependent functionality.
	- It was used to read the env vars of the docker file.
- **Docker-compose-wait**:
	- A small command line utility to wait for other docker images to be started while using docker-compose. It permits to wait for a fixed amount of seconds and/or to wait until a TCP port is open on a target image. 
	[ufoscout/docker-compose-wait](https://github.com/ufoscout/docker-compose-wait)

#### Create indexes in DB
We added a compound indexes for the low cardinality columns (status = 3, gender = 2, type = 2).
```sql
ALTER TABLE `watches` ADD INDEX( `status`, `gender`, `type`);
```
After that, we saw some improvements for searching watches with joins selection.

Example:
- query execution before:
```sql
SELECT * FROM `watches` WHERE `type` = "watch" AND `gender` = "woman" AND `status` = "current";
 
 Showing rows 0 - 24 (208 total, Query took 0.0026 seconds.)
```
- query execution after:
```sql
SELECT * FROM `watches` WHERE `type` = "watch" AND `gender` = "woman" AND `status` = "current";
 
  Showing rows 0 - 24 (208 total, Query took 0.0010 seconds.)
```

We measure the following speedup:
- speedup = 0.0026 / 0.0010 = 2.6