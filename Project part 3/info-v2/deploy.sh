#!/bin/bash
echo "Create Lambda functions"
sh ./create_lambdas.sh
echo "\n"
echo "---------------------------------------------------------------------------"
echo "\n"
echo "Create AWS API gateway"
API=$(sh create_api_gateway.sh | tail -1)
echo "\n"
echo "---------------------------------------------------------------------------"
echo "\n"
echo "Initialize the database (may takes some time)..."
python3 initialize_dynamodb.py
echo "\n"
echo "---------------------------------------------------------------------------"
echo "\n"
echo "Deploy public AWS API gateway"
echo "\n"
echo "---------------------------------------------------------------------------"
echo "\n"
aws apigateway create-deployment --rest-api-id $API --region eu-west-1 --stage-name infov2
echo $API
