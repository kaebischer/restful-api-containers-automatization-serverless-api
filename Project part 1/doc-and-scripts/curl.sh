#!/bin/bash 

curl -u cloud:computing -X POST -H 'Content-Type: application/json' http://localhost:5000/info/v1/watch -d '{"sku": "ztest","type": "watch","status": "old","gender": "man","year": 2021,"dial_material": "string","dial_color": "string","case_material": "string","case_form": "string","bracelet_material": "string","movement": "string"}'
curl -u cloud:computing -X POST -H 'Content-Type: application/json' http://localhost:5000/info/v1/watch -d '{"sku": "ztest","type": "watch","status": "old","gender": "nonbinary","year": 2021,"dial_material": "string","dial_color": "string","case_material": "string","case_form": "string","bracelet_material": "string","movement": "string"}'

curl -u cloud:computing -X GET http://localhost:5000/info/v1/watch/ztest
curl -u cloud:computing -X GET http://localhost:5000/info/v1/watch/ztestnonexistant

curl -u cloud:computing -X POST -H 'Content-Type: application/json' http://localhost:5000/info/v1/watch/ztest -d '{"sku": "ytest","type": "watch","status": "old","gender": "man","year": 2021,"dial_material": "string","dial_color": "string","case_material": "string","case_form": "string","bracelet_material": "string","movement": "string"}'
curl -u cloud:computing -X POST -H 'Content-Type: application/json' http://localhost:5000/info/v1/watch/ytest -d '{"sku": "ytest","type": "watch","status": "nonexistingstatus","gender": "man","year": 2021,"dial_material": "string","dial_color": "string","case_material": "string","case_form": "string","bracelet_material": "string","movement": "string"}'
curl -u cloud:computing -X POST -H 'Content-Type: application/json' http://localhost:5000/info/v1/watch/ytestnonexistant -d '{"sku": "ytest","type": "watch","status": "old","gender": "man","year": 2021,"dial_material": "string","dial_color": "string","case_material": "string","case_form": "string","bracelet_material": "string","movement": "string"}'

curl -u cloud:computing -X DELETE http://localhost:5000/info/v1/watch/ytest
curl -u cloud:computing -X DELETE http://localhost:5000/info/v1/watch/ytestnonexistant

curl -u cloud:computing -X GET http://localhost:5000/info/v1/watch/complete-sku/WW2117

curl -u cloud:computing -X GET -H 'Content-Type: application/json' http://localhost:5000/info/v1/watch/find -d '{"sku": "WW2117", "status": "old", "year": 2006}'