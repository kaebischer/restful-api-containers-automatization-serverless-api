# Cloud Computing 2021 Assignments - Project

## Watch Application
This is a RESTful application written in Python/Flask, with a given YAML specifications. It accesses a MySQL (informations) and a NoSQL (images) databases of watches.
The deployment is done in Google Cloud using Kubernetes and DockerHub.

They are two microservices:
- info-v1
- image-v1

### Getting started
Everything was tested on Ubuntu-mate 20.10 virtual machine. 

#### Requirements
##### Launch service with existing Google Cloud SQL Instance
To use your Google Cloud SQL Instance, please modify ENVs in the all.yml file (lines 21-28).
##### Launch service by creating a new Google Cloud SQL Instance
The MySQL database needs to be configured manually in Google Cloud SQL service. 
The connection with the pods is then established via private IP and specific login indicated into the ```all.yml``` file as an environment variables.
Steps for the configuration:
1. We created a MySQL Instance on Google Cloud Platform.
	- https://console.cloud.google.com/sql/choose-instance-engine
	- We chose MySQL v.8.0
	- Machine type: lightweight, 1vCPU, SSD storage 10GB
	- Connection: Public IP
2. Create a new bucket in Cloud Storage to put temporarily the dump file of the database.
3. Migrate MySQL data (watches.sql) with database migration service.
	- Creation of DB + import
4. Get the public IP address of the Cloud SQL Instance and add it in the all.yml file.
5. In the Cloud SQL Instances, configure the Authorized networks in the Connections settings.
	- Add network: 0.0.0.0/0
	Warning: this will authorized all external IP address to make requests to the current instance. (This of course not secured for a real production application)

The following commands are used to run everything in order to build and deploy all services:

```shell
sh build.sh
sh deploy.sh
```

These commands will perform the following:
- Build the docker images for the two microservices (info-v1, image-v1)
- Tag and push the two images in DockerHub and then the orchestration of the deployement (deployements + services + ingress) is handled by Kubernetes, via the description of the all.yml file.

#### Microservices access

- info-v1: http://34.117.51.19/info/v1/
	- *username* = cloud
	- *password* = computing
- image-v1: http://34.117.51.19/image/v1/
	
### Rolling updates

We can manually perform a rolling update for each service by executing:

1. `kubectl set image deployments/info-ms info-ms=aebiscka/info-ms:v2`

2. `kubectl set image deployments/image-ms image-ms=aebiscka/image-ms:v2`

We tried to make minor v2 that we have pushed on DockerHub and we the above commands we have seen the changes apply on the cluster.

Another method would be to change the ENV image of the container in the all.yml file with the new tag version of the image and then to run the command `kubectl apply -f all.yml`.


### Testing examples
These examples are accessible from everywhere.
- info-v1: 
	- http://34.117.51.19/info/v1/watch/CAR5A91.FT6162
- image-v1:
	- http://34.117.51.19/image/v1/watch/CAR5A91.FT6162 (returns 202 with an image)
	- http://34.117.51.19/image/v1/watch/CAR5A91.XXXXXX (returns 404 without an image)


### Features & utils
Here are the different modules used to achive the requirements of the project2 (we have listed only new modules not used in the previous part):
- **flask_cachecontrol**: A light-weight library to conveniently set Cache-Control headers on the response. Decorate view functions with cache_for, cache, or dont_cache decorators. Makes use of Flask response.cache_control.
	- We used this module to set proper Cache header of 1 hour.
- **requests_etag_cache**: It is an ETag cache for pyhton requests library.
	- We used this module to cache the ETag received during the request of the image resource.
- **requests_cache**: It is a transparent, persistent HTTP cache for the python requests library. It’s a convenient tool to use with web scraping, consuming REST APIs, slow or rate-limited sites, or any other scenario in which you’re making lots of requests that are expensive and/or likely to be sent more than once.
	- We used this module to cache the responses locally for better performance.
