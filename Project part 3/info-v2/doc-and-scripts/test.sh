#!/bin/bash

./doc-and-scripts/curl.sh > ./doc-and-scripts/output-of-requests.txt

if diff -q ./doc-and-scripts/output-of-requests.txt ./doc-and-scripts/correct-requests.txt; then
echo 
echo "=== SUCCESS: All curl commands return the correct responses! ==="
echo 
else
echo 
echo "=== FAILURE: Some curl commands return incorrect responses! ==="
echo 
fi