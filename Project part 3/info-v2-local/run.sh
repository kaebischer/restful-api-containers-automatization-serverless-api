#!/bin/bash
echo "Create docker network"
docker network create local-ddb-network
echo "\n"
echo "---------------------------------------------------------------------------"
echo "\n"
echo "Run docker container for dynamodb"
docker run -d -p 8000:8000 --network=local-ddb-network --name dynamodb amazon/dynamodb-local
echo "\n"
echo "---------------------------------------------------------------------------"
echo "\n"
echo "Initialize the database (may takes some time)..."
python3 initialize_dynamodb.py
echo "\n"
echo "---------------------------------------------------------------------------"
echo "\n"
echo "Start the API"
echo "\n"
echo "---------------------------------------------------------------------------"
echo "\n"
sam local start-api --docker-network local-ddb-network
